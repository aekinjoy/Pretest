import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'question1';
  result = false;
  textValue: any;
  selectData: any = "isPrime";
  onKeyUpEvent(event: any) {
    console.log(event.target.value);
  }

  inputText(event: any) {
    var inputValue = parseFloat(event.target.value);
    inputValue = Math.round(inputValue);
    if(inputValue >=0){
    event.target.value = inputValue;
    this.textValue = inputValue;
    }else{
      this.textValue =0;
      event.target.value = null;
    }
    this.calculate();
  }
  selectOption(event: any) {
    this.selectData = event.target.value;
    this.calculate();

  }
  calculate() {
    if (this.selectData == "isPrime") {
      this.result = this.isPrime(this.textValue);
    } else {
      this.result = this.isFibonacci(this.textValue);
    }


  }
  isPrime(num: any) {
    for (var i = 2; i < num; i++) {
      if (num % i === 0) {
        return false;
      }
    }
    return num > 1;
  }
  
  isFibonacci(num: any) {

    var test1 = Math.sqrt(5 * Math.pow(num, 2) + 4);
    var test2 = Math.sqrt(5 * Math.pow(num, 2) - 4);

    if (test1 == Math.floor(test1) || test2 == Math.floor(test2))
     return true;
    else
      return false;

  }


}
