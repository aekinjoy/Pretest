import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
    selector: 'input[numbersOnly]'
})
export class NumberDirective {

    constructor(private _el: ElementRef) { }

    @HostListener('input', ['$event']) onInputTextChange(event) {
        const initalValue = this._el.nativeElement.value;
       
        if (initalValue.indexOf('-') == 0 ) {
            this._el.nativeElement.value = initalValue.replace(/[^0-9.-]*/g, '');
        } else {
            this._el.nativeElement.value = initalValue.replace(/[^0-9.]*/g, '');
        }

        if (initalValue !== this._el.nativeElement.value) {
            event.stopPropagation();
        } else {
           
            if (initalValue.indexOf('-') == 0 && initalValue.length >1) {
                this._el.nativeElement.value = 1;
            }
            // else{
            //     var inputValue = parseFloat(initalValue);
            //     inputValue = Math.round(inputValue);
            //     console.log('initalValue',inputValue);
            //      if(inputValue >=0){
            //         this._el.nativeElement.value = inputValue;
            //      }
            // }
         
           
            
        }
    }

}