import { Component } from '@angular/core';
import { ApiService } from './utility/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'question2';
  filterText: string;
  categories: any = [];
  constructor(
    private apiService: ApiService
  ) { }

  async ngOnInit() {
    this.categories = await this.apiService.get('https://api.publicapis.org/categories');
    console.log(this.categories);
  }

  filterDatatable(val: any) {
    this.filterText = val;
  }

}
