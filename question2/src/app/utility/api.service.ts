import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: Http,
  ) { }

  async get(url: string) {
  
    
    return new Promise(resolve => {
      this.http.get(url).toPromise().then((data) => {
        const res = data.json();
        resolve(res);


      }).catch((err) => {
        console.log(err)
      });
    });
  }
}
