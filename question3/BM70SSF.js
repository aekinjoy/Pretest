
const Nightmare = require('nightmare')
const cheerio = require('cheerio');

const nightmare = Nightmare({ show: true })
const url = 'https://codequiz.azurewebsites.net/';

nightmare
  .goto(url)
  .wait('body')
  .click('input')
  .evaluate(() => document.querySelector('body').innerHTML)
  .end()
  .then(response => {
    console.log(getData(response));
  }).catch(err => {
    console.log(err);
  });

let getData = html => {
  data = '';
  const $ = cheerio.load(html);
  $('table tr').each((i, elem) => {
    if ($(elem).find("td:eq(0)").text().trim() == 'BM70SSF') {
      data = $(elem).find("td:eq(1)").text();
    }

  });
  return data;
}